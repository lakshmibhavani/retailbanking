package com.bankingapp.bankapplication.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.model.Admin;
import com.bankingapp.bankapplication.repository.AccountHolderRepository;
import com.bankingapp.bankapplication.repository.AdminRepository;

@RestController
@CrossOrigin
public class LoginController {
	@Autowired
	AdminRepository adminRepository;
	@Autowired
	AccountHolderRepository accountHolderRepository;
	@GetMapping("/admin")	
		public Map<String, Boolean> accessVerify(@RequestBody Admin admin) {
		Map<String, Boolean> response = new HashMap<>();
		Optional<Admin> admin1 = adminRepository.findById(1);
		Admin admin2 = admin1.get();
		if(admin2.getAdminName().equals(admin.getAdminName())&&
				admin2.getAdminpassword().equals(admin.getAdminpassword())) 
		{
		response.put("granted", Boolean.TRUE);
		return response; 
		}
		else response.put("denied", Boolean.FALSE);
		return response; 
}
	@GetMapping(value = "/accholder")
	public boolean accHolderAccess(@RequestBody AccountHolder accountHolder) {
		String name = accountHolderRepository.findExtAccHolderByName(accountHolder.getAccountHolderName());
		if(accountHolder.getAccountHolderPassword().equals(accountHolderRepository.findAccHolderPassword(name))) {
		return true;
		}
		else 
			return false;
	}
}