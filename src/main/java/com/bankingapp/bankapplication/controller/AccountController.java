package com.bankingapp.bankapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.model.FundTransfer;
import com.bankingapp.bankapplication.model.SavingsAccount;
import com.bankingapp.bankapplication.model.Transaction;
import com.bankingapp.bankapplication.repository.AccountRepository;
import com.bankingapp.bankapplication.repository.TransactionRepository;
import com.bankingapp.bankapplication.service.AccountService;


@RestController
public class AccountController {
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	AccountService accountService;
	
	@PostMapping(value = "/createaccount")                     // TO CREATE ACCOUNT FOR AN ACCOUNT HOLDER
	public Account addAccountHolder(@RequestBody AccountHolder accountHolder) {
		return accountService.addAccountHolder(accountHolder);
	}
	
	@GetMapping(value = "/getaccbynum/{accountId}")     // TO GET ACCOUNT DETAILS USING ACC.NUMBER
	public Account getAccount(@PathVariable("accountId") int accountId) {
		return accountService.getAccount(accountId);
	}
	@GetMapping(value = "/getallaccounts/{accountHolderId}")    // TO GET ALL ACCOUNTS OF AN ACC.HOLDER
	public List<Account> getByIdUser(@PathVariable("accountHolderId") int accountHolderId) {
	
		return accountService.getByIdUser(accountHolderId);
	}
	@PutMapping(value = "/updateaccount/{accId}")          //TO UPDATE ACCOUNT DETAILS
	public Account updateAccount(@PathVariable("accId") int accountId,@RequestBody SavingsAccount account) {
		return accountService.updateAccount(accountId, account);
	}
	@PutMapping("/deposit/{depAccId}/{depAmount}/")   // TO DEPOSIT MONEY INTO ACCOUNT
	public ResponseEntity<Transaction> depositAmount(@PathVariable(value = "depAccId") int depAccId,
			@PathVariable(value = "depAmount") double depAmount) {
		return accountService.depositAmount(depAccId, depAmount);
	}
	@PutMapping("/withdraw/{witAccId}/{witAmount}/")  // TO WITHDRAW MONEY FROM AN ACCOUNT
	public ResponseEntity<Transaction> withdraw(@PathVariable(value = "witAccId") int witAccId,
			@PathVariable(value = "witAmount") double witAmount) {
		return accountService.withdraw(witAccId, witAmount);
	}
	@PutMapping("/transfer")   // TO TRANSFER MONEY FROM AN ACCOUNT TO ANOTHER ACCOUNT
	public ResponseEntity<Transaction> transfer(@RequestBody FundTransfer fundTransfer) {
		return accountService.transfer(fundTransfer);
	}
}
