package com.bankingapp.bankapplication.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.repository.AccountRepository;
import com.bankingapp.bankapplication.service.AccountHolderService;

@RestController
@CrossOrigin
public class AccountHolderController {
	@Autowired
	AccountHolderService accountHolderService;

	@Autowired
	AccountRepository accountRepository;

	@PostMapping(value = "/addaccountholder")             // TO ADD A NEW ACC.HOLDER
	public AccountHolder createAccountHolder(@RequestBody AccountHolder accountHolder) {

		return accountHolderService.createAccountHolder(accountHolder);

	}

	@GetMapping(value = "/getaccholderbyname/{name}")  // TO GET ALL ACC.HOLDERS STARTING WITH A NAME
	public List<AccountHolder> getAccHolderByAccName(@PathVariable("name") String accountHolderName) {
		return accountHolderService.findAccHolderByName(accountHolderName);

	}

	@GetMapping(value = "/getaccholderbynum/{accnum}")  //TO GET AN ACCHOLDER USING ACC.NUMBER
	public Optional<AccountHolder> getAccHolderbyAccNo(@PathVariable("accnum") int account_id) {
		return accountHolderService.getAccHolderbyAccNo(account_id);

	}

	@GetMapping(value = "/getById/{holderid}") //TO GET ACC.HOLDER USING ACCHOLDER.ID
	public Optional<AccountHolder> getByIdUser(@PathVariable("holderid") int accountHolderId) {

		return accountHolderService.getByIdUser(accountHolderId);
	}

	@PutMapping("/accountHolder/{id}")  // TO UPDATE DETAILS OF AN ACC.HOLDER
	public ResponseEntity<String> updateaccountHolder(@PathVariable(value = "id") int accountHolderId,
			@RequestBody AccountHolder accountHolder) {
		return accountHolderService.updateaccountHolder(accountHolderId, accountHolder);
	}

	@DeleteMapping("/accountHolder/{id}")  // TO DELETE ACC HOLDER
	public Map<String, Boolean> deleteaccountHolder(@PathVariable(value = "id") int accountHolderId) {
		return accountHolderService.deleteaccountHolder(accountHolderId);
	}
}