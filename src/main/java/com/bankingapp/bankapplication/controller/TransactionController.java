package com.bankingapp.bankapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bankingapp.bankapplication.model.Transaction;
import com.bankingapp.bankapplication.service.TransactionService;

@RestController
public class TransactionController {
	@Autowired
	TransactionService transactionService;
	@GetMapping(value = "/getministatment/{accountId}") // TO GET MINI STATMENT FOR LAST 3 TRANSACTIONS
	public List<Transaction> getMiniTransactions(@PathVariable("accountId") int accountId) {
		return transactionService.getMiniTransactions(accountId);
	}
	@GetMapping(value = "/getdetailedstatment/{accountId}/{txntype}") // TO GET DETAILED TXN OF SPECIFIC TYPE
	public List<Transaction> getDetailedTransactions(@PathVariable("accountId") int accountId,
			@PathVariable("txntype") String transaction_type) {
		return transactionService.getDetailedTransactions(accountId, transaction_type);
	}
	
}
