package com.bankingapp.bankapplication.model;

public class FundTransfer {
	private int fromAccId;
	private int toAccId;
	private double transferAmount;

	public FundTransfer() {
	}

	public FundTransfer(int fromAccId, int toAccId, double transferAmount) {
		super();
		this.fromAccId = fromAccId;
		this.toAccId = toAccId;
		this.transferAmount = transferAmount;
	}

	public int getFromAccId() {
		return fromAccId;
	}

	public void setFromAccId(int fromAccId) {
		this.fromAccId = fromAccId;
	}

	public int getToAccId() {
		return toAccId;
	}

	public void setToAccId(int toAccId) {
		this.toAccId = toAccId;
	}

	public double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}

}
