package com.bankingapp.bankapplication.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "transaction")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TRANSACTION_TYPE", discriminatorType = DiscriminatorType.STRING, length = 3)
public abstract class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int transactionId;
	@Column
	private Date transactionDate;
	@Column
	private double transactionamount;
	@ManyToOne(optional = false)
	@JoinColumn(name = "accountId")
	@JsonIgnoreProperties(value = "accountHolder")
	private Account account;
	@Column
	private int destAccountId;
	@Column
	private String txnMsg;
	@Column(name = "TRANSACTION_TYPE", nullable = false, updatable = false, insertable = false)
	private String TRANSACTION_TYPE;

	
	public Transaction() {
		
	}

	public Transaction(Date transactionDate, double transactionamount, Account account, String txnMsg) {
		super();
		this.transactionDate = transactionDate;
		this.transactionamount = transactionamount;
		this.account = account;
		this.txnMsg = txnMsg;
	}

	public Transaction(Date transactionDate, double transactionamount, Account account, int destAccountId,
			String txnMsg) {
		super();
		this.transactionDate = transactionDate;
		this.transactionamount = transactionamount;
		this.account = account;
		this.destAccountId = destAccountId;
		this.txnMsg = txnMsg;
	}

	

	public String getTxnMsg() {
		return txnMsg;
	}

	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}

	public String getTRANSACTION_TYPE() {
		return TRANSACTION_TYPE;
	}

	public void setTRANSACTION_TYPE(String tRANSACTION_TYPE) {
		TRANSACTION_TYPE = tRANSACTION_TYPE;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTransactionamount() {
		return transactionamount;
	}

	public void setTransactionamount(double transactionamount) {
		this.transactionamount = transactionamount;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public int getDestAccountId() {
		return destAccountId;
	}

	public void setDestAccountId(int destAccountId) {
		this.destAccountId = destAccountId;
	}

}
