package com.bankingapp.bankapplication.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="accountholder")
public class AccountHolder {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int accountHolderId;
	 @Column
	private String accountHolderName;
	 @Column
	private String accountHolderPassword;
	 @Column
	private String accountHolderAddress;
	 @Column
	private long accountHolderMobile;
	 @Column
	private String accountHolderEmail;
	 @Column
	private long accountHolderAadhar;
	 @Column
	private String accountHolderPan;
	

	public AccountHolder() {
	}

	public int getAccountHolderId() {
		return accountHolderId;
	}

	public void setAccountHolderId(int accountHolderId) {
		this.accountHolderId = accountHolderId;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderPassword() {
		return accountHolderPassword;
	}

	public void setAccountHolderPassword(String accountHolderPassword) {
		this.accountHolderPassword = accountHolderPassword;
	}

	public String getAccountHolderAddress() {
		return accountHolderAddress;
	}

	public void setAccountHolderAddress(String accountHolderAddress) {
		this.accountHolderAddress = accountHolderAddress;
	}

	public long getAccountHolderMobile() {
		return accountHolderMobile;
	}

	public void setAccountHolderMobile(long accountHolderMobile) {
		this.accountHolderMobile = accountHolderMobile;
	}

	public String getAccountHolderEmail() {
		return accountHolderEmail;
	}

	public void setAccountHolderEmail(String accountHolderEmail) {
		this.accountHolderEmail = accountHolderEmail;
	}

	public long getAccountHolderAadhar() {
		return accountHolderAadhar;
	}

	public void setAccountHolderAadhar(long accountHolderAadhar) {
		this.accountHolderAadhar = accountHolderAadhar;
	}

	public String getAccountHolderPan() {
		return accountHolderPan;
	}

	public void setAccountHolderPan(String accountHolderPan) {
		this.accountHolderPan = accountHolderPan;
	}

	

	public AccountHolder(String accountHolderName, String accountHolderPassword, String accountHolderAddress,
			long accountHolderMobile, String accountHolderEmail, long accountHolderAadhar, String accountHolderPan) {
		super();
		this.accountHolderName = accountHolderName;
		this.accountHolderPassword = accountHolderPassword;
		this.accountHolderAddress = accountHolderAddress;
		this.accountHolderMobile = accountHolderMobile;
		this.accountHolderEmail = accountHolderEmail;
		this.accountHolderAadhar = accountHolderAadhar;
		this.accountHolderPan = accountHolderPan;
	}

	
}