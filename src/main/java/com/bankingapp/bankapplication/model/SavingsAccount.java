package com.bankingapp.bankapplication.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(value = "SA")
public class SavingsAccount extends Account implements Serializable{
	

	
	private static final long serialVersionUID = 1L;

	public SavingsAccount() {
		super();
	}

	public SavingsAccount(AccountHolder accountHolder) {
		super(accountHolder);
	}
	public SavingsAccount(double accountBalance) {
		super(accountBalance);
	}
	public SavingsAccount(double accountBalance, AccountHolder accountHolder) {
		super(accountBalance, accountHolder);
		
	}

	
}
