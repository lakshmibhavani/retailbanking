package com.bankingapp.bankapplication.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name="account")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ACCOUNT_TYPE", discriminatorType = DiscriminatorType.STRING, length = 2)
public abstract class Account {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int accountId;
	@Column
	private double accountBalance;
	@ManyToOne(optional = false )
	@JoinColumn(name = "accountHolderId")
	private AccountHolder accountHolder;
	@Column(name="ACCOUNT_TYPE", nullable=false, updatable=false, insertable=false)
	private String ACCOUNT_TYPE;
	

	public String getACCOUNT_TYPE() {
		return ACCOUNT_TYPE;
	}


	public void setACCOUNT_TYPE(String aCCOUNT_TYPE) {
		ACCOUNT_TYPE = aCCOUNT_TYPE;
	}


	

	
	public Account() {
		super();
		
	}


	public Account(AccountHolder accountHolder) {
		super();
		this.accountHolder = accountHolder;
	}

	public Account(double accountBalance) {
		super();
		this.accountBalance = accountBalance;
	}
	public Account(double accountBalance, AccountHolder accountHolder) {
	
		this.accountBalance = accountBalance;
		this.accountHolder = accountHolder;
	}


	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public AccountHolder getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolder accountHolder) {
		this.accountHolder = accountHolder;
	}

	
}
