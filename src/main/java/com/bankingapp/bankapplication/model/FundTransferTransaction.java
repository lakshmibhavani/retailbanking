package com.bankingapp.bankapplication.model;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
@Entity
@DiscriminatorValue(value = "TFR")
public class FundTransferTransaction extends Transaction implements Serializable{

	
	private static final long serialVersionUID = 1L;

	public FundTransferTransaction() {
		
	}

	public FundTransferTransaction(Date transactionDate, double transactionamount, Account account, int destAccountId,
			String txnMsg) {
		super(transactionDate, transactionamount, account, destAccountId, txnMsg);
		
	}

	

	



	
}
