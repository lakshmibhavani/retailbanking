package com.bankingapp.bankapplication.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
@Entity
@DiscriminatorValue(value = "DPT")
public class DepositTransaction extends Transaction implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public DepositTransaction() {
		super();
	}

	public DepositTransaction(Date transactionDate, double transactionamount, Account account, String txnMsg) {
		super(transactionDate, transactionamount, account, txnMsg);
	}

	

	
}
