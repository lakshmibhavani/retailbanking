package com.bankingapp.bankapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.model.Transaction;

public interface AccountHolderRepository extends JpaRepository<AccountHolder, Integer> {

	@Query(value ="select * from accountholder where account_holder_name like %?1%",
			nativeQuery = true)
	  Optional<List<AccountHolder>> findAccHolderByName(String accountHolderName);
	@Query(value ="select account_holder_name from accountholder where account_holder_name=?1",
			nativeQuery = true)
	  String findExtAccHolderByName(String accountHolderName);
	@Query(value ="select account_holder_password from accountholder where account_holder_name=?1",
			nativeQuery = true)
	  String findAccHolderPassword(String accountHolderName);
}
