package com.bankingapp.bankapplication.repository;

import org.springframework.data.repository.CrudRepository;

import com.bankingapp.bankapplication.model.Admin;

public interface AdminRepository extends CrudRepository<Admin, Integer> {
}
