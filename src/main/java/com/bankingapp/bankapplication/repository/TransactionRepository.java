package com.bankingapp.bankapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

	@Query(value = "SELECT * FROM transaction WHERE account_id=?1 OR dest_account_id=?1 ORDER BY "
			+ "transaction_date DESC LIMIT 0, 3", nativeQuery = true)
	  Optional<List<Transaction>> findMiniTransactions(int accountId);
	
	@Query(value = "SELECT * FROM transaction WHERE transaction_type=?2 AND "
			+ "(account_id=?1 OR dest_account_id=?1) ORDER BY "
			+ "transaction_date DESC LIMIT 0, 3", nativeQuery = true)
	  Optional<List<Transaction>> findDetailedTransactions(int accountId,String transaction_type);
}
