package com.bankingapp.bankapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.AccountHolder;

public interface AccountRepository extends JpaRepository<Account, String>{

	 @Query(value = "SELECT * FROM account WHERE account_id=?1 ", nativeQuery = true)
	Optional<Account> findByAccountId(int account_id);
	 @Query(value = "SELECT * FROM account WHERE account_type=?1 ", nativeQuery = true)
		Optional<Account> findByAccountType(String accountType);
	 
	 @Query(value = "SELECT * FROM account WHERE account_holder_id=?1 ", nativeQuery = true)
		Optional<List<Account>> findByAccountHolderId(int accountHolderId);
	 @Query(value = "SELECT account_holder_id FROM account WHERE account_id=?1 ", nativeQuery = true)
		Optional<Integer> findAccHolderByAccNum(int account_id);
	
}
