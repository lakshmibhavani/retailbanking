package com.bankingapp.bankapplication.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.model.FundTransfer;
import com.bankingapp.bankapplication.model.SavingsAccount;
import com.bankingapp.bankapplication.model.Transaction;

public interface AccountService {
	public Account addAccountHolder(AccountHolder accountHolder);
	public Account getAccount(int accountId); 
	public List<Account> getByIdUser(int accountHolderId);
	public Account updateAccount(int accountId, SavingsAccount account);
	public ResponseEntity<Transaction> depositAmount( int depAccId,double depAmount);
	public ResponseEntity<Transaction> withdraw( int witAccId,double witAmount);
	public ResponseEntity<Transaction> transfer(FundTransfer fundTransfer);
}
