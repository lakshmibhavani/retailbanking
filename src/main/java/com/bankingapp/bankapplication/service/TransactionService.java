package com.bankingapp.bankapplication.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.bankingapp.bankapplication.model.Transaction;

public interface TransactionService {
	public List<Transaction> getMiniTransactions(int accountId);
	public List<Transaction> getDetailedTransactions(int accountId,String transaction_type);
	public Transaction createTransaction(Transaction transaction);
}
