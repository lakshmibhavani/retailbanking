package com.bankingapp.bankapplication.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bankingapp.bankapplication.ExceptionHandling.AccountHolderNotFoundException;
import com.bankingapp.bankapplication.ExceptionHandling.InvalidAccountNumberException;
import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.repository.AccountHolderRepository;
import com.bankingapp.bankapplication.repository.AccountRepository;
@Service
@Transactional
public class AccountHolderServiceImpl implements AccountHolderService {
	@Autowired
	AccountHolderRepository accountHolderRepository;
	@Autowired
	AccountRepository accountRepository;
	@Override
	public AccountHolder createAccountHolder(AccountHolder accountHolder) {
		
		return accountHolderRepository.save(accountHolder);
	}

	@Override
	public List<AccountHolder> findAccHolderByName(String accountHolderName) throws AccountHolderNotFoundException{
		List<AccountHolder> accHolders = new ArrayList<>();
		accHolders = accountHolderRepository.findAccHolderByName(accountHolderName)
				.orElseThrow(() -> new AccountHolderNotFoundException("The account holder name entered is invalid" ));
		return accHolders;
	}

	@Override
	public Optional<AccountHolder> getAccHolderbyAccNo(int account_id) throws AccountHolderNotFoundException{
		Integer accountHolderId = accountRepository.findAccHolderByAccNum(account_id)
				.orElseThrow(() -> new AccountHolderNotFoundException("The account num entered is invalid" ));
		return getByIdUser(accountHolderId); 
	}

	@Override
	public Optional<AccountHolder> getByIdUser(int accountHolderId) throws AccountHolderNotFoundException {
		Optional<AccountHolder> accountHolder = accountHolderRepository.findById(accountHolderId);
		if(!accountHolder.isPresent()) {
			throw new AccountHolderNotFoundException("The account number entered is invalid");
		}
		return accountHolder;
	}

	@Override
	public ResponseEntity<String> updateaccountHolder(int accountHolderId, AccountHolder accountHolder) throws AccountHolderNotFoundException{
		Optional<AccountHolder> holder = accountHolderRepository.findById(accountHolderId);
		if(!holder.isPresent()) {
			throw new AccountHolderNotFoundException("The account number entered is invalid");
		}
		AccountHolder accholder = holder.get();
		accholder.setAccountHolderName(accountHolder.getAccountHolderName());
		accholder.setAccountHolderPassword(accountHolder.getAccountHolderPassword());
		accholder.setAccountHolderAddress(accountHolder.getAccountHolderAddress());
		accholder.setAccountHolderMobile(accountHolder.getAccountHolderMobile());
		accholder.setAccountHolderEmail(accountHolder.getAccountHolderEmail());
		accholder.setAccountHolderAadhar(accountHolder.getAccountHolderAadhar());
		accholder.setAccountHolderPan(accountHolder.getAccountHolderPan());
		accountHolderRepository.save(accholder);
		return ResponseEntity.ok("updated successfully");
	}

	@Override
	public Map<String, Boolean> deleteaccountHolder(int accountHolderId) throws AccountHolderNotFoundException {
		Optional<AccountHolder> holder = accountHolderRepository.findById(accountHolderId);
		if(!holder.isPresent()) {
			throw new AccountHolderNotFoundException("The account number entered is invalid");
		}
		AccountHolder accholder = holder.get();
		accountHolderRepository.delete(accholder);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
