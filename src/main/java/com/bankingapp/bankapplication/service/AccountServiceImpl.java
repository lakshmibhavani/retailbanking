package com.bankingapp.bankapplication.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import com.bankingapp.bankapplication.ExceptionHandling.InsufficientFundsException;
import com.bankingapp.bankapplication.ExceptionHandling.InvalidAccountNumberException;
import com.bankingapp.bankapplication.model.Account;
import com.bankingapp.bankapplication.model.AccountHolder;
import com.bankingapp.bankapplication.model.DepositTransaction;
import com.bankingapp.bankapplication.model.FundTransfer;
import com.bankingapp.bankapplication.model.FundTransferTransaction;
import com.bankingapp.bankapplication.model.SavingsAccount;
import com.bankingapp.bankapplication.model.Transaction;
import com.bankingapp.bankapplication.model.WithdrawlTransaction;
import com.bankingapp.bankapplication.repository.AccountRepository;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	TransactionService transactionService;

	@Override
	public Account addAccountHolder(AccountHolder accountHolder) {
		Account account = new SavingsAccount(accountHolder);

		System.out.println(accountHolder);

		Account account1 = accountRepository.save(account);
		return account1;
	}

	@Override
	public Account getAccount(int accountId) throws InvalidAccountNumberException{
		Optional<Account> account = accountRepository.findByAccountId(accountId);
		if(!account.isPresent()) {
			throw new InvalidAccountNumberException("The account number entered is invalid");
		}
		return account.get();
	}

	@Override
	public List<Account> getByIdUser(int accountHolderId) throws InvalidAccountNumberException{
		Optional<List<Account>> account = accountRepository.findByAccountHolderId(accountHolderId);
				if(!account.isPresent()) {
					throw new InvalidAccountNumberException("The account holder entered is invalid");
				}

		return account.get();
	}

	@Override
	public Account updateAccount(int accountId, SavingsAccount account) throws InvalidAccountNumberException{
		Optional<Account> account2 = accountRepository.findByAccountId(accountId);
		if(!account2.isPresent()) {
			throw new InvalidAccountNumberException("The account number entered is invalid");
		}
		Account acc = account2.get();
		acc.setAccountBalance(account.getAccountBalance());

		return accountRepository.save(acc);
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public ResponseEntity<Transaction> depositAmount(int depAccId, double depAmount)
			throws InvalidAccountNumberException{
		Optional<Account> account = accountRepository.findByAccountId(depAccId);
		if(!account.isPresent()) {
			
			throw new InvalidAccountNumberException("The account number entered is invalid");
		}
		Account acc = account.get();
		double balance = acc.getAccountBalance();
		acc.setAccountBalance(balance + depAmount);
		Account acc1 = accountRepository.save(acc);
		Transaction transaction = new DepositTransaction(new Date(), depAmount, acc1, "txn_dpt_sucess");
		transactionService.createTransaction(transaction);
		return ResponseEntity.ok(transaction);
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public ResponseEntity<Transaction> withdraw(int witAccId, double witAmount) throws
	InsufficientFundsException,InvalidAccountNumberException{
		Optional<Account> account = accountRepository.findByAccountId(witAccId);
		if(!account.isPresent()) {
			throw new InvalidAccountNumberException("The account number entered is invalid");
		}
		Account acc = account.get();
	 double balance = acc.getAccountBalance();
	 if(balance<witAmount) {
		 throw new InsufficientFundsException("insufficent funds to do withdrawl");
	 }
	 acc.setAccountBalance(balance - witAmount);
		Account acc1 = accountRepository.save(acc);
		Transaction transaction = new WithdrawlTransaction(new Date(), witAmount, acc1, "txn_wdl_success");
		transactionService.createTransaction(transaction);
		return ResponseEntity.ok(transaction);
	}

	@Override
	public ResponseEntity<Transaction> transfer(FundTransfer fundTransfer) throws
	InsufficientFundsException,InvalidAccountNumberException
	{
		
		Account wdl_account = accountRepository.findByAccountId(fundTransfer.getFromAccId())
				.orElseThrow(() -> new InvalidAccountNumberException("The account number entered is invalid" ));
		 double balance = wdl_account.getAccountBalance();
		 if(balance<fundTransfer.getTransferAmount()) {
			 throw new InsufficientFundsException("insufficent funds to do withdrawl");
		 }
		 wdl_account.setAccountBalance(balance - fundTransfer.getTransferAmount());
			accountRepository.save(wdl_account);
			
			Account dpt_account = accountRepository.findByAccountId(fundTransfer.getToAccId())
					.orElseThrow(() -> new InvalidAccountNumberException("The account number entered is invalid" ));
			double dpt_act_balance = dpt_account.getAccountBalance();
			dpt_account.setAccountBalance(dpt_act_balance + fundTransfer.getTransferAmount());
			accountRepository.save(dpt_account);
			Transaction transaction = new FundTransferTransaction(new Date(), fundTransfer.getTransferAmount(),
					wdl_account, fundTransfer.getToAccId(), "txn_tfr_success");
			transactionService.createTransaction(transaction);
		return ResponseEntity.ok(transaction);
	}

}
