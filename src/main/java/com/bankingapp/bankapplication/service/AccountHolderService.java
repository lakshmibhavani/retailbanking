package com.bankingapp.bankapplication.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.ResponseEntity;


import com.bankingapp.bankapplication.model.AccountHolder;

public interface AccountHolderService {
	public AccountHolder createAccountHolder(AccountHolder accountHolder);
	public List<AccountHolder> findAccHolderByName(String accountHolderName);
	public Optional<AccountHolder> getAccHolderbyAccNo(int account_id);
	public Optional<AccountHolder> getByIdUser(int accountHolderId);
	public ResponseEntity<String> updateaccountHolder(int accountHolderId,AccountHolder accountHolder);
	public Map<String, Boolean> deleteaccountHolder(int accountHolderId);
}
