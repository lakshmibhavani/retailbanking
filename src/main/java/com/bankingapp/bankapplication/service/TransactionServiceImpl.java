package com.bankingapp.bankapplication.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingapp.bankapplication.model.Transaction;
import com.bankingapp.bankapplication.repository.TransactionRepository;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	TransactionRepository transactionRepository;

	@Override
	public List<Transaction> getMiniTransactions(int accountId) {
		System.out.println(accountId + "print here");
		List<Transaction> holders = new ArrayList<>();
		holders = transactionRepository.findMiniTransactions(accountId).get();

		return holders;
	}

	@Override
	public List<Transaction> getDetailedTransactions(int accountId, String transaction_type) {
		System.out.println(accountId + "print here");
		List<Transaction> holders = new ArrayList<>();
		holders = transactionRepository.findDetailedTransactions(accountId, transaction_type).get();
		return holders;
	}

	@Override
	public Transaction createTransaction(Transaction transaction) {

		return transactionRepository.save(transaction);
	}

}
