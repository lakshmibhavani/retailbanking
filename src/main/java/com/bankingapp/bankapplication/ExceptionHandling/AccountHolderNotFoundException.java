package com.bankingapp.bankapplication.ExceptionHandling;

public class AccountHolderNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AccountHolderNotFoundException(String message) {
		super(message);
	}

}
